ARG DEP_IMAGE
FROM ${DEP_IMAGE}
ARG PREFIX
ARG SCRIPT_DIR
RUN echo $SCRIPT_DIR
COPY $SCRIPT_DIR/install.sh /tmp
RUN /tmp/install.sh
RUN rm /tmp/install.sh
