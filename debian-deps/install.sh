#!/bin/bash

set -x

YML2_DEPS='python3-lxml '
REP_DEPS='mercurial git '
BUILD_DEPS='build-essential automake libtool '
GRAL_DEPS='ca-certificates letsencrypt gnupg '
ENGINE_DEPS='uuid-dev libgpgme-dev libsqlite3-dev sqlite3 '
SEQUOIA_DEPS='rustc cargo clang make pkg-config nettle-dev libssl-dev '\
'capnproto python3-dev python3-setuptools python3-cffi python3-pytest'
PYTHON_DEPS='libboost-python-dev libboost-locale-dev'
DEPS=$YML2_DEPS$REP_DEPS$BUILD_DEPS$GRAL_DEPS$ENGINE_DEPS$SEQUOIA_DEPS
echo $DEPS

# echo "--------Installing system dependencies--------------"
if [ "$EUID" -ne 0 ]
then
    echo 'sudo required to install system dependencies'
    sudo apt update -yq
    sudo apt install -yq $DEPS
    sudo apt clean
else
    apt update -yq
    # general
    apt install -yq $DEPS
    apt clean
fi
