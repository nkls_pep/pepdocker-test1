#!/bin/bash

set -x

export PREFIX=${PREFIX:=/usr/local}

git clone https://gitlab.com/sequoia-pgp/sequoia.git
cd sequoia
git checkout pep-engine
make build-release PYTHON=disable
make install PYTHON=disable PREFIX=$PREFIX
cd ..
rm -rf sequoia $PREFIX/cargo $(whoami)/rustup $(whoami)/.cargo $(whoami)/.cache
